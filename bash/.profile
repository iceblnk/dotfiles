# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

if [ -n "$BASH_VERSION" ]; then
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$HOME/.local/bin:$PATH"
    PATH="$HOME/":$PATH
fi

#flutter
export PATH="$PATH":"$HOME/.pub-cache/bin"

# pywal
wal -R
export BAT_THEME="OneHalfDark"

# esp8266
export IDF_PATH="$HOME/esp/ESP8266_RTOS_SDK"

export VISUAL=vim
export EDITOR="$VISUAL"
export TERM=xterm-256color
